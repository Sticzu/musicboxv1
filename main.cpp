#include "musicboxvideo.h"

#include <QApplication>
#include <QStringList>
#include <QStringListModel>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    musicboxvideo w;
    w.show();
    return a.exec();
}
