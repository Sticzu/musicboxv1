#include "musicboxvideo.h"
#include "ui_musicboxvideo.h"
#include <QProcess>
#include <QStringList>
#include <QStringListModel>
#include <QtMultimedia>
#include <QtMultimediaWidgets>
#include <QPropertyAnimation>
#include <QObject>
#include <QtGui>
#include <QKeyEvent>
#include <QTimer>
#include <QMessageBox>

QString urlMusicDir = "/home/debian/musicbox/build-musicboxv1-Desktop-Debug/music/";

QStringListModel *listdiscmodel = new QStringListModel;
QStringListModel *listmusicmodel = new QStringListModel;
QStringListModel *listplaylistmodel = new QStringListModel;



QString indexstringdisc;
QString indexstringmusic;

float kredit = 0;
int oldsizedata = 0;
int sizedata = 0;


int alphatime = 0;
double alpha = 1;


musicboxvideo::musicboxvideo(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::musicboxvideo)
{
    ui->setupUi(this);



    player = new QMediaPlayer(this);
    player->setVolume(100);
    playlista = new QMediaPlaylist;
    player->setPlaylist(playlista);



    timerautoclosemsg = new QTimer;
    timerautoclosemsg->setInterval(4000);

    timer = new QTimer;
    timer->setInterval(500);
    timer->start();

    connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(tdc(qint64)));
    connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(tpc(qint64)));
    connect(player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus )), this, SLOT(pstop(QMediaPlayer::MediaStatus )));
    connect(timer, SIGNAL(timeout()), this, SLOT(tick()));
    connect(timerautoclosemsg, SIGNAL(timeout()), this, SLOT(autocloseendtime()));

   //this->centralWidget()->setStyleSheet("background-image: url('tlo.png')");
    QPixmap bkgnd("tlo.png");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, bkgnd);
    this->setPalette(palette);

    ui->valuemoney->setText(QString::number(kredit));


    QProcess urlmusic;
    //urlmusic.setWorkingDirectory(".");
    urlmusic.start("cat", QStringList()<<"urlmusic.txt" );
    urlmusic.waitForFinished();
    urlMusicDir = urlmusic.readAllStandardOutput();
    urlMusicDir.replace(10, "");


    //wylistuje pliki na listdicmodel
    QProcess listfiles;
    listfiles.setWorkingDirectory(urlMusicDir);
    listfiles.start("ls", QStringList()<<"-1" );
    listfiles.waitForFinished();
    QString stdOut = listfiles.readAllStandardOutput();
    listdisc = stdOut.split(10);
    listdisc.removeLast();
    listdiscmodel->setStringList(listdisc);
    ui->discview->setModel(listdiscmodel);

    ui->musicview->setStyleSheet("background-color: rgba( 0,0,0,0.60 ); ");
    ui->discview->setStyleSheet("background-color: rgba( 0,0,0,0.60 ); world-wrap: break-word");

    infolog("uruchomiono program");

}

musicboxvideo::~musicboxvideo()
{
    delete ui;
}


void musicboxvideo::on_discview_indexesMoved(const QModelIndexList &indexes)
{

}

void musicboxvideo::on_discview_activated(const QModelIndex &index)
{
     indexstringdisc = listdiscmodel->stringList().value(index.row());
     QProcess listfiles;
     listfiles.setWorkingDirectory(urlMusicDir+indexstringdisc);
     listfiles.start("ls", QStringList()<<"-1" );
     listfiles.waitForFinished();
     QString stdOut = listfiles.readAllStandardOutput();
     stdOut.remove(stdOut.length()-1, 1);
     listmusic = stdOut.split(10);



     QString outview = stdOut;
     outview.replace(".mp3", "");



     QStringList listmusicview = outview.split(10);

     listmusic.removeOne("image.png");
     listmusicview.removeOne("image.png");
     listmusicmodel->setStringList(listmusicview);
     ui->musicview->setModel(listmusicmodel);

     ui->musicview->setCurrentIndex(listmusicmodel->index(0));


      //wyświetlanie miniaturki
     QPixmap pixmap;
     pixmap.load(urlMusicDir+indexstringdisc+"/image.png");
     pixmap.scaled(800,600);

     ui->imagep->setPixmap(pixmap);
     ui->imagep->setMask(pixmap.mask());
     ui->imagep->setScaledContents(true);
     QString tmp = "background-image: url('"+urlMusicDir+indexstringdisc+"/image.png') 0 0 0 0 stretch stretch; bacground-clolor: rgba( 0,0,0,0.33);";


}

void musicboxvideo::addplaylistkey(  ) {

}

void musicboxvideo::infocentermessage( QString message ) {
   timerautoclosemsg->start();
    static QMessageBox windowtmp;
    msgbox = &windowtmp;
    msgbox->setStyleSheet("font-size: 40px; color: red; background-color: white");
    msgbox->setText(message);
    msgbox->setButtonText(1, "");
    //msgbox->setStandardButtons(QMessageBox::NoButton);
    //msgbox->removeButton(msgbox->defaultButton());
    msgbox->show();
}


void musicboxvideo::on_musicview_activated(const QModelIndex &index)
{
    if(kredit >= 1 &&  ui->musicview->currentIndex().isValid()) {
        indexstringmusic = listmusic.value(index.row());

        if(this->player->state() == QMediaPlayer::StoppedState) {
            playlista->clear();
            playlista->addMedia(QUrl::fromLocalFile(urlMusicDir+indexstringdisc+"/"+indexstringmusic));
            player->play();

        } else {
            playlista->addMedia(QUrl::fromLocalFile(urlMusicDir+indexstringdisc+"/"+indexstringmusic));
        }
        infolog( "wybrano utwór: " + indexstringdisc + "/ " +indexstringmusic );

        QString tmpname = indexstringmusic.replace(".mp3", "");
        listplay.append(indexstringdisc+"/"+indexstringmusic);
        listplaylistmodel->setStringList(listplay);
        ui->playlistview->setModel(listplaylistmodel);
        ui->trackreal->setText(listplay.first().split("/").last());

        infocentermessage( "<center><font color=lime>wybrano utwór:</font></center>\n" + indexstringmusic );

        kredit--; // odejmóje kredyty utworów
        ui->valuemoney->setText(QString::number(kredit));


    } else if( kredit <= 0 ) {
        infocentermessage( "Nie ma kredytów!" );
        infolog("brak kredytów, wybrano utwór: " + indexstringdisc + "/ " +indexstringmusic );
    }
}

void musicboxvideo::on_pushButton_clicked()
{



}

void musicboxvideo::movelistdisc(  int position ){
    ui->discview->setCurrentIndex(listdiscmodel->index(ui->discview->currentIndex().row()+position));
    if( ui->discview->currentIndex().isValid() ) {
        ui->discview->activated(ui->discview->currentIndex());
    }
}

void musicboxvideo::movelistmusic( int position ){
    ui->musicview->setCurrentIndex(listmusicmodel->index(ui->musicview->currentIndex().row()+position));
}


void musicboxvideo::keyPressEvent(QKeyEvent *event) {
    if( event->key() == Qt::Key_M ) {
        movelistdisc( 1 );

    } else if( event->key() == Qt::Key_N ) {
        movelistdisc( -1 );

    } else if( event->key() == Qt::Key_PageDown ) {
         movelistdisc( 10 );

    } else if( event->key() == Qt::Key_PageUp ) {
        movelistdisc( -10 );

    }else if( event->key() == Qt::Key_Period ) {
        movelistmusic(1);

    } else if( event->key() == Qt::Key_Comma ) {
          movelistmusic(-1);

    } else if( event->key() == Qt::Key_Z || event->key() == Qt::Key_Enter ) {   
        ui->musicview->activated(ui->musicview->currentIndex());

    } else if( event->key() == Qt::Key_R ) {
        listplay.removeFirst();
        listplaylistmodel->setStringList(listplay);
        ui->playlistview->reset();
        ui->playlistview->setModel(listplaylistmodel);

    }
    //alphatime = 0;
    //->setWindowOpacity(1.0000);


}

void musicboxvideo::tdc(qint64 position) {
    ui->progressBar->setMaximum(position);
}
void musicboxvideo::tpc(qint64 position) {
    ui->progressBar->setValue(position);
}
void musicboxvideo::pstop( QMediaPlayer::MediaStatus status ) {
    if( status == QMediaPlayer::EndOfMedia ) {
        listplay.removeFirst();
        listplaylistmodel->setStringList(listplay);
        ui->playlistview->reset();
        ui->playlistview->setModel(listplaylistmodel);
        if( listplay.count() > 1 ) {
            ui->trackreal->setText(listplay.first().split("/").last());
        } else {
            ui->trackreal->setText("brak utworu");
        }
    }
}

void musicboxvideo::keyReleaseEvent(QKeyEvent *event) {
    if( event->key() == Qt::Key_M ) {
        //ui->sourceinput->setText( "fasdfasdf" );
    }
}

void musicboxvideo::autocloseendtime() {
    msgbox->close();

    timerautoclosemsg->stop();
}

void musicboxvideo::tick() {


    QProcess catttyusb;
    catttyusb.setWorkingDirectory(".");
    catttyusb.start("cat", QStringList()<<"/tmp/filetty" );
    catttyusb.waitForFinished();
    QString out = catttyusb.readAllStandardOutput();

    if( !out.isEmpty() ) {
        out.replace(10, "");
        QString lastMoeny = out.at(out.count()-1);
        if( out.count() != oldsizedata ) {
            if( lastMoeny == "5" ) {
                kredit += 3;
                lastMoeny = "";
                infolog("wrzucono zł 5");
            } else if( lastMoeny == "2" ) {
                 kredit += 1;
                 lastMoeny = "";
                 infolog("wrzucono zł 2");
            } else if( lastMoeny == "1" ) {
                kredit += 0.5;
                lastMoeny = "";
                infolog("wrzucono zł 1");
           }
            if( kredit == 1 ) {
                infocentermessage( "<center><font color=green>możesz wybrać</font>\n<br><font>" + QString::number(kredit) + "</font><br><font color=green>utwór</font></center>" );

            } else if( kredit < 5 ) {
                infocentermessage( "<center><font color=green>możesz wybrać</font>\n<br><font>" + QString::number(kredit) + "</font><br><font color=green>utwory</font></center>" );
            } else {
                infocentermessage( "<center><font color=green>możesz wybrać</font>\n<br><font>" + QString::number(kredit) + "</font><br><font color=green>utworów</font></center>" );
            }
            oldsizedata = out.count();
        }
    }
     ui->valuemoney->setText(QString::number(kredit));
     ui->trackvalue->setText(QString::number(listplay.count()+1));

    //cieniowanie aplikacji
/*
    alphatime++;
    if( 0 ) {
        alpha -= 0.010;F
        this->setWindowOpacity(alpha);
    } else if( alphatime > 70) {
        if( alpha > 0.1 ) {
          alpha -= 0.005;
          this->setWindowOpacity(alpha);
        }
    } else {
        alpha = 1;
        this->setWindowOpacity(1.0000);
    }
*/


}

void musicboxvideo::infolog( QString massage ) {
    qDebug(qUtf8Printable(massage));
}
