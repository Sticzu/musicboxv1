#ifndef MUSICBOXVIDEO_H
#define MUSICBOXVIDEO_H

#include <QMainWindow>
#include <QStringList>
#include <QStringListModel>
#include <QtMultimedia>
#include <QtMultimediaWidgets>



QT_BEGIN_NAMESPACE
namespace Ui { class musicboxvideo; }
QT_END_NAMESPACE

class musicboxvideo : public QMainWindow
{
    Q_OBJECT

public:
    musicboxvideo(QWidget *parent = nullptr);
    ~musicboxvideo();
    QStringList listdisc;
    QStringList listmusic;
    QStringList listplay;
    QStringListModel *listdiscmodel = new QStringListModel;
    QStringListModel *listmusicmodel = new QStringListModel;
    QMediaPlaylist *playlista;
    QPropertyAnimation *animation;
    QMessageBox * msgbox;

    //void musicboxvideo::on_positionChanged(qint64 position);

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);


private slots:
    void on_discview_indexesMoved(const QModelIndexList &indexes);

    void on_discview_activated(const QModelIndex &index);

    void tdc(qint64 position);
    void tpc(qint64 position);
    void pstop(QMediaPlayer::MediaStatus status);

    void on_musicview_activated(const QModelIndex &index);

    void on_pushButton_clicked();
    void tick();

    void movelistdisc( int position );
    void movelistmusic( int position );
    void addplaylistkey();
    void autocloseendtime();
    void infocentermessage(QString message);
    void infolog( QString messgae );

private:
    Ui::musicboxvideo *ui;
    QMediaPlayer *player;
    QTimer *timer;
    QTimer *timerautoclosemsg;
};
#endif // MUSICBOXVIDEO_H
