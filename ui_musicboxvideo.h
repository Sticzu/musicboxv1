/********************************************************************************
** Form generated from reading UI file 'musicboxvideo.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MUSICBOXVIDEO_H
#define UI_MUSICBOXVIDEO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_musicboxvideo
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_6;
    QListView *musicview;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_5;
    QListView *playlistview;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout;
    QLabel *textakt;
    QLabel *trackreal;
    QProgressBar *progressBar;
    QFrame *line;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label;
    QGridLayout *gridLayout_2;
    QListView *discview;
    QGridLayout *gridLayout_5;
    QLabel *imagep;
    QVBoxLayout *verticalLayout;
    QLabel *kredytyl;
    QLabel *valuemoney;
    QLabel *label_4;
    QLabel *trackvalue;
    QMenuBar *menubar;

    void setupUi(QMainWindow *musicboxvideo)
    {
        if (musicboxvideo->objectName().isEmpty())
            musicboxvideo->setObjectName(QString::fromUtf8("musicboxvideo"));
        musicboxvideo->resize(1280, 1024);
        musicboxvideo->setFocusPolicy(Qt::NoFocus);
        musicboxvideo->setWindowOpacity(1.000000000000000);
        musicboxvideo->setAutoFillBackground(true);
        musicboxvideo->setStyleSheet(QString::fromUtf8(""));
        centralwidget = new QWidget(musicboxvideo);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(0);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        musicview = new QListView(centralwidget);
        musicview->setObjectName(QString::fromUtf8("musicview"));
        musicview->setMinimumSize(QSize(0, 0));
        musicview->setMaximumSize(QSize(16777215, 1000000));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush1(QColor(168, 119, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        QBrush brush2(QColor(67, 36, 37, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Base, brush2);
        QBrush brush3(QColor(83, 37, 38, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush3);
        QBrush brush4(QColor(255, 255, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush4);
        QBrush brush5(QColor(0, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::HighlightedText, brush5);
        QBrush brush6(QColor(188, 91, 92, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
        QBrush brush7(QColor(255, 255, 255, 128));
        brush7.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        QBrush brush8(QColor(119, 119, 119, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        QBrush brush9(QColor(60, 140, 230, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
        QBrush brush10(QColor(0, 0, 0, 128));
        brush10.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        musicview->setPalette(palette);
        QFont font;
        font.setFamily(QString::fromUtf8("Sans Serif"));
        font.setPointSize(16);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        musicview->setFont(font);
        musicview->setFocusPolicy(Qt::NoFocus);
        musicview->setStyleSheet(QString::fromUtf8(""));
        musicview->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        musicview->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        musicview->setEditTriggers(QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        musicview->setTextElideMode(Qt::ElideRight);
        musicview->setProperty("isWrapping", QVariant(false));
        musicview->setWordWrap(true);

        gridLayout_6->addWidget(musicview, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush11(QColor(0, 50, 0, 127));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush11);
        QBrush brush12(QColor(95, 55, 55, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        QBrush brush13(QColor(255, 181, 192, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::HighlightedText, brush13);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette1.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush13);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette1.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        label_5->setPalette(palette1);
        QFont font1;
        font1.setFamily(QString::fromUtf8("Sans Serif"));
        font1.setPointSize(16);
        font1.setBold(false);
        font1.setWeight(50);
        label_5->setFont(font1);
        label_5->setStyleSheet(QString::fromUtf8("background-color: rgba( 0,50,0,0.50 );"));

        horizontalLayout_2->addWidget(label_5);


        gridLayout_6->addLayout(horizontalLayout_2, 6, 0, 1, 1);

        playlistview = new QListView(centralwidget);
        playlistview->setObjectName(QString::fromUtf8("playlistview"));
        playlistview->setMaximumSize(QSize(16777215, 100));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        QBrush brush14(QColor(5, 22, 0, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Base, brush14);
        palette2.setBrush(QPalette::Active, QPalette::Window, brush3);
        QBrush brush15(QColor(26, 172, 0, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Highlight, brush15);
        palette2.setBrush(QPalette::Active, QPalette::HighlightedText, brush13);
        palette2.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette2.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette2.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Base, brush14);
        palette2.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette2.setBrush(QPalette::Inactive, QPalette::Highlight, brush15);
        palette2.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush13);
        palette2.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette2.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette2.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette2.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        palette2.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette2.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush13);
        palette2.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette2.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        playlistview->setPalette(palette2);
        QFont font2;
        font2.setFamily(QString::fromUtf8("Sans Serif"));
        font2.setPointSize(12);
        font2.setBold(false);
        font2.setItalic(false);
        font2.setWeight(50);
        playlistview->setFont(font2);
        playlistview->setFocusPolicy(Qt::NoFocus);
        playlistview->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        playlistview->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        playlistview->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        playlistview->setTextElideMode(Qt::ElideRight);
        playlistview->setVerticalScrollMode(QAbstractItemView::ScrollPerItem);

        gridLayout_6->addWidget(playlistview, 8, 0, 1, 1);

        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("C059 [urw]"));
        font3.setPointSize(14);
        font3.setBold(true);
        font3.setWeight(75);
        label_3->setFont(font3);
        label_3->setStyleSheet(QString::fromUtf8("color: red; background-color: rgba( 0,50,0,0.50 );"));
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_3, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        textakt = new QLabel(centralwidget);
        textakt->setObjectName(QString::fromUtf8("textakt"));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette3.setBrush(QPalette::Active, QPalette::Text, brush);
        palette3.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette3.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette3.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette3.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        palette3.setBrush(QPalette::Active, QPalette::HighlightedText, brush13);
        palette3.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette3.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette3.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette3.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette3.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette3.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette3.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush13);
        palette3.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette3.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette3.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette3.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette3.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette3.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette3.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush13);
        palette3.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette3.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        textakt->setPalette(palette3);
        QFont font4;
        font4.setFamily(QString::fromUtf8("Sans Serif"));
        font4.setPointSize(18);
        font4.setBold(false);
        font4.setWeight(50);
        textakt->setFont(font4);
        textakt->setStyleSheet(QString::fromUtf8("background-color: rgba( 0,50,0,0.50 );"));

        horizontalLayout->addWidget(textakt);

        trackreal = new QLabel(centralwidget);
        trackreal->setObjectName(QString::fromUtf8("trackreal"));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette4.setBrush(QPalette::Active, QPalette::Text, brush);
        palette4.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette4.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette4.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette4.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        palette4.setBrush(QPalette::Active, QPalette::HighlightedText, brush13);
        palette4.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette4.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette4.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette4.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette4.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette4.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette4.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette4.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush13);
        palette4.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette4.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette4.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette4.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette4.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette4.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette4.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette4.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush13);
        palette4.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette4.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        trackreal->setPalette(palette4);
        trackreal->setFont(font4);
        trackreal->setStyleSheet(QString::fromUtf8("background-color: rgba( 0,50,0,0.50 );"));

        horizontalLayout->addWidget(trackreal);


        gridLayout_6->addLayout(horizontalLayout, 3, 0, 1, 1);

        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setMinimumSize(QSize(0, 20));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush);
        palette5.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush14);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette5.setBrush(QPalette::Active, QPalette::Highlight, brush15);
        palette5.setBrush(QPalette::Active, QPalette::HighlightedText, brush13);
        palette5.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette5.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush14);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette5.setBrush(QPalette::Inactive, QPalette::Highlight, brush15);
        palette5.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush13);
        palette5.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette5.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette5.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        palette5.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette5.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush13);
        palette5.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette5.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        progressBar->setPalette(palette5);
        progressBar->setMaximum(1000);
        progressBar->setValue(0);
        progressBar->setInvertedAppearance(false);

        gridLayout_6->addWidget(progressBar, 5, 0, 1, 1);

        line = new QFrame(centralwidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_6->addWidget(line, 1, 0, 1, 1);


        verticalLayout_3->addLayout(gridLayout_6);


        gridLayout->addLayout(verticalLayout_3, 1, 1, 6, 1);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMaximumSize(QSize(16777215, 70));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette6.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette6.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        palette6.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette6.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette6.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette6.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette6.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette6.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette6.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        palette6.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        label_2->setPalette(palette6);
        QFont font5;
        font5.setFamily(QString::fromUtf8("URW Bookman [urw]"));
        font5.setPointSize(48);
        font5.setItalic(true);
        label_2->setFont(font5);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_2, 0, 1, 1, 1);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(16777215, 70));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette7.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette7.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        palette7.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette7.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette7.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette7.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette7.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette7.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette7.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        palette7.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        label->setPalette(palette7);
        label->setFont(font5);
        label->setLayoutDirection(Qt::LeftToRight);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));

        verticalLayout_4->addLayout(gridLayout_2);

        discview = new QListView(centralwidget);
        discview->setObjectName(QString::fromUtf8("discview"));
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Active, QPalette::Text, brush);
        palette8.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette8.setBrush(QPalette::Active, QPalette::Base, brush2);
        palette8.setBrush(QPalette::Active, QPalette::Window, brush3);
        palette8.setBrush(QPalette::Active, QPalette::Highlight, brush4);
        palette8.setBrush(QPalette::Active, QPalette::HighlightedText, brush5);
        palette8.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette8.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette8.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette8.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette8.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette8.setBrush(QPalette::Inactive, QPalette::Window, brush3);
        palette8.setBrush(QPalette::Inactive, QPalette::Highlight, brush4);
        palette8.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush5);
        palette8.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette8.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette8.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette8.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette8.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette8.setBrush(QPalette::Disabled, QPalette::Base, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::Window, brush3);
        palette8.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette8.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush5);
        palette8.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette8.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        discview->setPalette(palette8);
        QFont font6;
        font6.setFamily(QString::fromUtf8("Sans Serif"));
        font6.setPointSize(22);
        font6.setBold(false);
        font6.setItalic(false);
        font6.setWeight(50);
        discview->setFont(font6);
        discview->setFocusPolicy(Qt::NoFocus);
        discview->setStyleSheet(QString::fromUtf8(""));
        discview->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        verticalLayout_4->addWidget(discview);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        imagep = new QLabel(centralwidget);
        imagep->setObjectName(QString::fromUtf8("imagep"));
        imagep->setMinimumSize(QSize(300, 300));
        imagep->setMaximumSize(QSize(300, 300));

        gridLayout_5->addWidget(imagep, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        kredytyl = new QLabel(centralwidget);
        kredytyl->setObjectName(QString::fromUtf8("kredytyl"));
        kredytyl->setMaximumSize(QSize(16777215, 100));
        QPalette palette9;
        palette9.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette9.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette9.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette9.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette9.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette9.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        palette9.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette9.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette9.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette9.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette9.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette9.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette9.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette9.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette9.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette9.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette9.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette9.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        kredytyl->setPalette(palette9);
        QFont font7;
        font7.setFamily(QString::fromUtf8("URW Bookman [urw]"));
        font7.setPointSize(48);
        font7.setBold(false);
        font7.setItalic(true);
        font7.setWeight(50);
        kredytyl->setFont(font7);
        kredytyl->setStyleSheet(QString::fromUtf8("background-color: rgba( 0,50,0,0.50 )"));
        kredytyl->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(kredytyl);

        valuemoney = new QLabel(centralwidget);
        valuemoney->setObjectName(QString::fromUtf8("valuemoney"));
        valuemoney->setMaximumSize(QSize(16777215, 100));
        QPalette palette10;
        QBrush brush16(QColor(255, 0, 0, 255));
        brush16.setStyle(Qt::SolidPattern);
        palette10.setBrush(QPalette::Active, QPalette::WindowText, brush16);
        palette10.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette10.setBrush(QPalette::Active, QPalette::Text, brush16);
        palette10.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette10.setBrush(QPalette::Active, QPalette::ButtonText, brush16);
        palette10.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette10.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette10.setBrush(QPalette::Active, QPalette::Highlight, brush12);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette10.setBrush(QPalette::Active, QPalette::PlaceholderText, brush16);
#endif
        palette10.setBrush(QPalette::Inactive, QPalette::WindowText, brush16);
        palette10.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette10.setBrush(QPalette::Inactive, QPalette::Text, brush16);
        palette10.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette10.setBrush(QPalette::Inactive, QPalette::ButtonText, brush16);
        palette10.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette10.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette10.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette10.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush16);
#endif
        palette10.setBrush(QPalette::Disabled, QPalette::WindowText, brush16);
        palette10.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette10.setBrush(QPalette::Disabled, QPalette::Text, brush16);
        palette10.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette10.setBrush(QPalette::Disabled, QPalette::ButtonText, brush16);
        palette10.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette10.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette10.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette10.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush16);
#endif
        valuemoney->setPalette(palette10);
        QFont font8;
        font8.setFamily(QString::fromUtf8("URW Bookman [urw]"));
        font8.setPointSize(100);
        font8.setBold(false);
        font8.setItalic(true);
        font8.setWeight(50);
        valuemoney->setFont(font8);
        valuemoney->setStyleSheet(QString::fromUtf8("color: red; background-color: rgba( 0,50,0,0.50 );"));
        valuemoney->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(valuemoney);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QPalette palette11;
        palette11.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette11.setBrush(QPalette::Active, QPalette::Text, brush);
        palette11.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette11.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette11.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette11.setBrush(QPalette::Active, QPalette::Highlight, brush4);
        palette11.setBrush(QPalette::Active, QPalette::HighlightedText, brush5);
        palette11.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette11.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette11.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette11.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette11.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette11.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette11.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette11.setBrush(QPalette::Inactive, QPalette::Highlight, brush4);
        palette11.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush5);
        palette11.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette11.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette11.setBrush(QPalette::Disabled, QPalette::WindowText, brush8);
        palette11.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette11.setBrush(QPalette::Disabled, QPalette::Text, brush8);
        palette11.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette11.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette11.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette11.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette11.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush5);
        palette11.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette11.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        label_4->setPalette(palette11);
        label_4->setFont(font1);
        label_4->setLayoutDirection(Qt::LeftToRight);
        label_4->setStyleSheet(QString::fromUtf8("background-color: rgba( 0,50,0,0.50 );"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        trackvalue = new QLabel(centralwidget);
        trackvalue->setObjectName(QString::fromUtf8("trackvalue"));
        QPalette palette12;
        palette12.setBrush(QPalette::Active, QPalette::WindowText, brush4);
        palette12.setBrush(QPalette::Active, QPalette::Button, brush11);
        palette12.setBrush(QPalette::Active, QPalette::Text, brush4);
        palette12.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette12.setBrush(QPalette::Active, QPalette::ButtonText, brush4);
        palette12.setBrush(QPalette::Active, QPalette::Base, brush11);
        palette12.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette12.setBrush(QPalette::Active, QPalette::Highlight, brush4);
        palette12.setBrush(QPalette::Active, QPalette::HighlightedText, brush5);
        palette12.setBrush(QPalette::Active, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette12.setBrush(QPalette::Active, QPalette::PlaceholderText, brush7);
#endif
        palette12.setBrush(QPalette::Inactive, QPalette::WindowText, brush4);
        palette12.setBrush(QPalette::Inactive, QPalette::Button, brush11);
        palette12.setBrush(QPalette::Inactive, QPalette::Text, brush4);
        palette12.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette12.setBrush(QPalette::Inactive, QPalette::ButtonText, brush4);
        palette12.setBrush(QPalette::Inactive, QPalette::Base, brush11);
        palette12.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette12.setBrush(QPalette::Inactive, QPalette::Highlight, brush4);
        palette12.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush5);
        palette12.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette12.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush7);
#endif
        palette12.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette12.setBrush(QPalette::Disabled, QPalette::Button, brush11);
        palette12.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette12.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette12.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette12.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette12.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        palette12.setBrush(QPalette::Disabled, QPalette::Highlight, brush9);
        palette12.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush5);
        palette12.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette12.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush10);
#endif
        trackvalue->setPalette(palette12);
        QFont font9;
        font9.setFamily(QString::fromUtf8("Century Schoolbook L"));
        font9.setPointSize(50);
        font9.setBold(true);
        font9.setWeight(75);
        trackvalue->setFont(font9);
        trackvalue->setStyleSheet(QString::fromUtf8("color: yellow; background-color: rgba( 0,50,0,0.50 )"));
        trackvalue->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(trackvalue);


        gridLayout_5->addLayout(verticalLayout, 0, 1, 1, 1);


        verticalLayout_4->addLayout(gridLayout_5);


        gridLayout->addLayout(verticalLayout_4, 0, 0, 7, 1);

        musicboxvideo->setCentralWidget(centralwidget);
        menubar = new QMenuBar(musicboxvideo);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1280, 23));
        musicboxvideo->setMenuBar(menubar);

        retranslateUi(musicboxvideo);

        QMetaObject::connectSlotsByName(musicboxvideo);
    } // setupUi

    void retranslateUi(QMainWindow *musicboxvideo)
    {
        musicboxvideo->setWindowTitle(QCoreApplication::translate("musicboxvideo", "musicboxvideo", nullptr));
        label_5->setText(QCoreApplication::translate("musicboxvideo", "lista utwor\303\263w w kolecje:", nullptr));
        label_3->setText(QCoreApplication::translate("musicboxvideo", "~~~~~~~~~~~ Odtwarzacz ~~~~~~~~~~~", nullptr));
        textakt->setText(QCoreApplication::translate("musicboxvideo", "Aktualnie odtwarzany: ", nullptr));
        trackreal->setText(QString());
        progressBar->setFormat(QCoreApplication::translate("musicboxvideo", "%p%", nullptr));
        label_2->setText(QCoreApplication::translate("musicboxvideo", "A potem utw\303\263r", nullptr));
        label->setText(QCoreApplication::translate("musicboxvideo", "Wybierz p\305\202yt\304\231", nullptr));
        imagep->setText(QString());
        kredytyl->setText(QCoreApplication::translate("musicboxvideo", "Kredyty:", nullptr));
        valuemoney->setText(QCoreApplication::translate("musicboxvideo", "0 ", nullptr));
        label_4->setText(QCoreApplication::translate("musicboxvideo", "Twoja piosenka jest w kolejce:", nullptr));
        trackvalue->setText(QCoreApplication::translate("musicboxvideo", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class musicboxvideo: public Ui_musicboxvideo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MUSICBOXVIDEO_H
